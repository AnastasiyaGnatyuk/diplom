﻿using System;
using System.IO;
using System.Threading.Tasks;
using Predict2.Model;
using static Predict2.Model.ConsoleHelpers;



namespace Predict2
{
    public class Class1
    {
        
        public void push(FileStream file)
        {
            var tagsTsv = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Predict\\assets\\inputs\\data", "tags.tsv");
            var imagesFolder = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Predict\\assets\\inputs\\data");
            var imageClassifierZip = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Predict\\assets\\inputs", "imageClassifier.zip");

            try
            {
                var modelScorer = new ModelScorer(tagsTsv, imagesFolder, imageClassifierZip);
                modelScorer.ClassifyImages(file);
            }
            catch (Exception ex)
            {
                ConsoleWriteException(ex.Message);
            }

            ConsolePressAnyKey();
        }
    }
}
