﻿using System;
using System.IO;
using System.Threading.Tasks;
using Predict.Model;
using static Predict.Model.ConsoleHelpers;

namespace Predict
{
    public class Program
    {
        public void push()
        {
            var tagsTsv = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Predict\\assets\\inputs\\data", "tags.tsv");
            var imagesFolder = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Predict\\assets\\inputs\\data");
            var imageClassifierZip = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Predict\\assets\\inputs", "imageClassifier.zip");

            try
            {
                var modelScorer = new ModelScorer(tagsTsv, imagesFolder, imageClassifierZip);
                modelScorer.ClassifyImages();
            }
            catch (Exception ex)
            {
                ConsoleWriteException(ex.Message);
            }

            ConsolePressAnyKey();
        }

        static void Main(string[] args)
        {
            var tagsTsv = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Predict\\assets\\inputs\\data", "tags.tsv");
            var imagesFolder = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Predict\\assets\\inputs\\data");
            var imageClassifierZip = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Predict\\assets\\inputs", "imageClassifier.zip");

            try
            {
                var modelScorer = new ModelScorer(tagsTsv, imagesFolder, imageClassifierZip);
                modelScorer.ClassifyImages();
            }
            catch (Exception ex)
            {
                ConsoleWriteException(ex.Message);
            }

            ConsolePressAnyKey();
        }
    }
}

