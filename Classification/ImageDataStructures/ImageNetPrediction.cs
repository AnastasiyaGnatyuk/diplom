﻿using Classification.ModelScorer;
using Microsoft.ML.Data;

namespace Classification.ImageDataStructures
{
    public class ImageNetPrediction
    {
        [ColumnName(TFModelScorer.InceptionSettings.outputTensorName)]
        public float[] PredictedLabels;
    }
}
