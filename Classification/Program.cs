﻿using System;
using System.IO;
using Classification.ModelScorer;

namespace Classification
{
    
    public class Program
    {
        public void push(string s,string name)
        {
            var str = ModelHelpers.getstring();
            var assetsPath = Path.Combine(str, @"..\..\..\assets");

            var tagsTsv = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Classification\\assets\\inputs\\images\\tags.tsv");
            //var imagesFolder = imgPicture.Source;
            var imagesFolder = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Classification\\assets\\inputs\\images");

            //using (StreamWriter sw = new StreamWriter("C:\\Users\\gnaty\\source\\repos\\Diplom\\Clasiffication\\assets\\inputs\\images\\tags.tsv", false, System.Text.Encoding.Default))
            //{
            //    sw.Write(name);
            //}

            var inceptionPb = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Classification\\assets\\inputs\\inception\\tensorflow_inception_graph.pb");
            var labelsTxt = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Classification\\assets\\inputs\\inception\\imagenet_comp_graph_label_strings.txt");

            var customInceptionPb = Path.Combine(assetsPath, "inputs", "inception_custom", "model_tf.pb");
            var customLabelsTxt = Path.Combine(assetsPath, "inputs", "inception_custom", "labels.txt");

            try
            {
                var modelScorer = new TFModelScorer(tagsTsv, imagesFolder, inceptionPb, labelsTxt);
                modelScorer.Score();

            }
            catch (Exception ex)
            {
                ConsoleHelpers.ConsoleWriteException(ex.Message);
            }

            ConsoleHelpers.ConsolePressAnyKey();
        }
        static void Main(string[] args)
        {
            var str = ModelHelpers.getstring();
            var assetsPath = Path.Combine(str, @"..\..\..\assets");

            var tagsTsv = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Classification\\assets\\inputs\\images\\tags.tsv");
            //var imagesFolder = imgPicture.Source;
            var imagesFolder = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Classification\\assets\\inputs\\images");
            var inceptionPb = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Classification\\assets\\inputs\\inception\\tensorflow_inception_graph.pb");
            var labelsTxt = Path.Combine("C:\\Users\\gnaty\\source\\repos\\Diplom\\Classification\\assets\\inputs\\inception\\imagenet_comp_graph_label_strings.txt");

            var customInceptionPb = Path.Combine(assetsPath, "inputs", "inception_custom", "model_tf.pb");
            var customLabelsTxt = Path.Combine(assetsPath, "inputs", "inception_custom", "labels.txt");

            try
            {
                var modelScorer = new TFModelScorer(tagsTsv, imagesFolder, inceptionPb, labelsTxt);
                modelScorer.Score();

            }
            catch (Exception ex)
            {
                ConsoleHelpers.ConsoleWriteException(ex.Message);
            }

            ConsoleHelpers.ConsolePressAnyKey();
        }
    }
}
