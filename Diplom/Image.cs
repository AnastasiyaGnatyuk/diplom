﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Diplom
{
    public class Image: INotifyPropertyChanged
    {
        private string imagepath;

        public string ImagePath
        {
            get { return imagepath; }
            set
            {
                imagepath = value;
                OnPropertyChanged("ImagePath");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
