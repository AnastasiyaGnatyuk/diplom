﻿using Diplom.ModelScorer;
using Microsoft.ML.Data;

namespace Diplom.ImageDataStructures
{
    public class ImageNetPrediction
    {
        [ColumnName(TFModelScorer.InceptionSettings.outputTensorName)]
        public float[] PredictedLabels;
    }
}
