﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using Microsoft.Data.DataView;
using Microsoft.ML;
using Microsoft.ML.Core.Data;
using Microsoft.ML.Data;
using Microsoft.ML.ImageAnalytics;
using Microsoft.ML.Trainers;

namespace Diplom
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>

    public class DataSet
    {
        public string data;
        public string name;
    }
   
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofdPicture = new OpenFileDialog();
            ofdPicture.Filter =
                "Image files|*.bmp;*.jpg;*.gif;*.png;*.tif|All files|*.*";
            ofdPicture.FilterIndex = 1;

            if (ofdPicture.ShowDialog() == true)
                imgPicture.Source =
                    new BitmapImage(new Uri(ofdPicture.FileName));
            DataSet dataSet = new DataSet();
            dataSet.data = ofdPicture.FileName;
            dataSet.name = ofdPicture.SafeFileName;

            //main m = new main();
            //m.pusck();

            MLContext mlContext = new MLContext(seed: 1);
            ITransformer loadedModel;

            using (var fileStream = new FileStream("C:\\Users\\gnaty\\source\\repos\\Diplom\\Diplom\\bin\\Debug\\assets\\outputs\\imageClassifier.zip", FileMode.Open))
                loadedModel = mlContext.Model.Load(fileStream);
            
        }



        private void escButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
